
# Navigating freenode IRC anti-spam restrictions

Several of the IRC channels in which I participate have been able to run for 
years in a fairly newcomer-friendly fashion. Spammers came through from time 
to time, but each time the impact was brief and we were able to roll with 
it. 

Recently, though, a persistent, recurring series of attacks necessitated 
that we lock things down a little bit. Our challenge has been to do so in a 
way that keeps the channels useful to us.

This remains an ongoing process, but this is my attempt to document some of 
what I've found, so far, in part to solidify my progress, to help remind me 
as time passes, and to let others see what I've done so that we can continue 
to help and teach each other.

Original portions of this document (ie, those not included under fair use
from channel services help pages) are licensed for reuse via CC-BY-SA 4.0.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

# What we did

In brief, we restricted access to the channel, allowing participation only 
from IRC accounts (`nicks`, short for `nicknames`) that are registered and 
identified.

We did this in two ways, in turn. First, we set the channel `+r` which keeps 
unregistered nicks from joining the channel in the first place. This mostly 
works to keep the spam out, but is too aggressive, since it excludes 
newcomers who have not yet registered or learned what registration entails 
or required. Bots that provide bridging functions to keep the channel 
traffic synchronized with other chat services (eg, Matrix), or that provide 
other services (eg, the quote functionality, meeting tracking, and URL title 
lookup) are often difficult to register and identify to ChanServ.

So, we removed `r` and put the channel `+q $~a` which allows unregistered 
nicks to join, but silences them by removing `voice` from them. These nicks 
will then be visible to those already in the channel (eg, via the `/names` 
command) and can be given voice.

# Understanding what we did

There are at least four classes of settings to bear in mind when trying to 
manage a channel in Freenode:

* user modes
* channel modes
* mode locks
* flags
* access

These settings interact in ways I'm still discovering and learning to 
navigate.

They interact through a number of mechanisms. First is at the IRC server 
level. Commands issued in the user's IRC client, conveyed by the client to 
the IRC server software, and effected by the server software. 

Next are a pair of IRC `services` which are, essentially, robot IRC clients 
which can be directed to issue commands to the IRC server on behalf of the 
channel's various users according to a variety of conditions. These two services 
are `NickServ` and `ChanServ`.

One interacts with NickServ and ChanServ by sending them private messages, 
aka "pm's".

## NickServ

With `NickServ`, two commands are key: One for registration, one for authentication.

Registration binds a password and an email address to a nick.

So, for example, to register one would do something like:

    /msg nickserv register mynewnick mypassword my.email@example.com

`NickServ` will send a confirmatory email and request that you take steps to 
demonstrate to it that you received that email. 

Once that's done, your nick is registered.

Once registered, you can then authenticate to `NickServ`:

    /msg nickserv identify mypassword

where `mypassword` in the `identify` command matches the password one gave 
during the `register` process further above.

You will need to authenticate with the `identify` command every time you connect. You may be able to 
configure your client to do this for you.

When authenticated, `NickServ` can then, essentially, "vouch" for you with the IRC server and with `ChanServ`.

To understand what `ChanServ` does, it's best to understand the range of 
settings it is able to put into effect on behalf of the channel admins, 
including `user modes` and `channel modes`.

## User Modes

https://freenode.net/kb/answer/usermodes

## Channel Modes

https://freenode.net/kb/answer/channelmodes

## extbans

https://freenode.net/kb/answer/extbans



## ChanServ

I have not been able to find a web page documenting the following, so I'm 
copying-and-pasting the results from the respective `help` commands from `ChanServ` 

### Flags 

    help flags

    ***** ChanServ Help *****
    Help for FLAGS:
    The FLAGS command allows for the granting/removal of channel
    privileges on a more specific, non-generalized level. It
    supports nicknames, groups and hostmasks as targets.
     
    When only the channel argument is given, a listing of
    permissions granted to users will be displayed.
     
    Syntax: FLAGS <#channel>
     
    Otherwise, an access entry is modified. A modification may be
    specified by a template name (changes the access to the
    template) or a flags change (starts with + or -). See the
    TEMPLATE help entry for more information about templates.
     
    If you are not a founder, you may only manipulate flags you
    have yourself, and may not edit users that have flags you
    don't have. For this purpose, +v grants the ability to grant
    +V, +o grants the ability to grant +O, and +r grants the
    ability to grant +b.
     
     
    As of Atheme 7.0, there are now extended entity targets
    which allow you to match chanacs against a situation instead
    of against a nickname, hostmask or group.
     
    Available exttargets are:
    $chanacs:#channel - Any user with channel access in the given channel
                          (including hostmasks).
     
    If you do not have +f you may still remove your own access
    with -*.
     
    Syntax: FLAGS <#channel> [nickname|hostmask|group template]
    Syntax: FLAGS <#channel> [nickname|hostmask|group flag_changes]
     
    Permissions:
        +v - Enables use of the voice/devoice commands.
        +V - Enables automatic voice.
        +o - Enables use of the op/deop commands.
        +O - Enables automatic op.
        +s - Enables use of the set command.
        +i - Enables use of the invite and getkey commands.
        +r - Enables use of the unban command.
        +R - Enables use of the recover, sync and clear commands.
        +f - Enables modification of channel access lists.
        +t - Enables use of the topic and topicappend commands.
        +A - Enables viewing of channel access lists.
        +S - Marks the user as a successor.
        +F - Grants full founder access.
        +b - Enables automatic kickban.
        +e - Exempts from +b and enables unbanning self.
     
    The special permission +* adds all permissions except +b, +S, and +F.
    The special permission -* removes all permissions including +b and +F.
     
    Examples:
        /msg ChanServ FLAGS #foo
        /msg ChanServ FLAGS #foo foo!*@bar.com VOP
        /msg ChanServ FLAGS #foo foo!*@bar.com -V+oO
        /msg ChanServ FLAGS #foo foo!*@bar.com -*
        /msg ChanServ FLAGS #foo foo +oOtsi
        /msg ChanServ FLAGS #foo TroubleUser!*@*.troubleisp.net +b
        /msg ChanServ FLAGS #foo !baz +*
    ***** End of Help *****

### access

    help access
    ***** ChanServ Help *****
    Help for ACCESS:
     
    The ACCESS command allows displaying and changing
    privileges on channels.
     
    The LIST subcommand displays a list of users and
    their privileges.
     
    Syntax: ACCESS <#channel> LIST
     
    The ADD subcommand adds a user to the access list
    or changes their privileges if they were already on
    the access list.
     
    If the level field is specified, it specifies the
    privileges to grant, see the FLAGS and TEMPLATE
    help files. If not, the user will be given
    privileges appropriate for day-to-day management
    of the channel.
     
    Syntax: ACCESS <#channel> ADD <nickname> [level]
     
    The DEL subcommand removes a user from the access list.
     
    Syntax: ACCESS <#channel> DEL <nickname>
     
    Examples:
        /msg ChanServ ACCESS #foo LIST
        /msg ChanServ ACCESS #foo ADD bar OP
        /msg ChanServ ACCESS #foo DEL bar
     
    See also: FLAGS, TEMPLATE
    ***** End of Help *****


### set mlock

    help set mlock
    ***** ChanServ Help *****
    Help for SET MLOCK:
     
    MLOCK (or "mode lock") allows you to enforce a set
    of modes on a channel.  This can prevent abuse in cases
    such as +kl. It can also make it harder to fight evil
    bots, be careful. Locked modes can be seen by anyone
    recreating the channel (this includes keys).
     
    Syntax: SET <#channel> MLOCK [modes]
     
    Examples: (some may use modes your ircd does not support)
        /msg ChanServ SET #foo MLOCK +nt-lk
        /msg ChanServ SET #foo MLOCK +inst-kl
        /msg ChanServ SET #c MLOCK +ntk c
        /msg ChanServ SET #foo MLOCK +ntcjf-kl 2:30 #overflow
        /msg ChanServ SET #overflow MLOCK +mntF-kljf
        /msg ChanServ SET #foo1 MLOCK +ntlL 40 #foo2
    ***** End of Help *****


